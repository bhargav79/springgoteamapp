<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Available Matches</h2>

		<form:form id="registrationForm" method="post" action="register"
			commandName="nominationBean">

			<table>
				<tr>
					<th></th>
					<th>Match Details</th>
					<th>Date</th>
				</tr>

				<c:if test="${not empty matchInfos}">
					<c:forEach var="matchInfo" items="${matchInfos}" varStatus="status">

						<tr>
							<td>${matchInfo.id}</td>
							<td>${matchInfo.matchDetails}</td>
							<td>${matchInfo.matchDate}</td>
						</tr>
					</c:forEach>
				</c:if>
			</table>
		</form:form>

	</tiles:putAttribute>
</tiles:insertDefinition>