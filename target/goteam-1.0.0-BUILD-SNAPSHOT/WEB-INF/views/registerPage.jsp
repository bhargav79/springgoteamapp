<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<title>Go Team</title>
</head>
<body>
	<h1 align="center">Go Team Registration</h1>
	<br />
	<p>${registerStatus}</p>

	<form:form id="registrationForm" method="post" action="register"
		commandName="userRegister">

		<form:errors path="*" />

		<form:label path="firstName">Enter your First Name</form:label>
		<form:input name="firstName" path="firstName" />
		<form:errors path="firstName" />
		<br>
		<form:label path="lastName">Please enter Last Name</form:label>
		<form:input name="lastName" path="lastName" />
		<form:errors path="lastName" />
		<br>
		<form:label path="dob">Please enter Date of Birth (mm/dd/yyyy)</form:label>
		<form:input name="dob" path="dob" />
		<form:errors path="dob" />
		<br>
		<form:label path="userName">Enter your User Name</form:label>
		<form:input name="userName" path="userName" />
		<form:errors path="userName" />
		<input type="submit" value="Check Username" name="_checkUser" />
		<br>
		<form:label path="password">Please enter your password</form:label>
		<form:password name="password" path="password" />
		<form:errors path="password" />
		<br>
		<form:label path="emailId">Please enter your email id</form:label>
		<form:input name="emailId" path="emailId" />
		<form:errors path="emailId" />
		<br>
		<br>
		<input type="submit" value="Back" name="_back" />
		<input type="submit" value="Register" name="_register" />
	</form:form>
</body>
</html>
