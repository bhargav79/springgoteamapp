<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Team Details</h2>
		<br />

		<form:form id="GetDetailsForm" method="post"
			action="team_details_submit" commandName="nominationBean">

			<form:select path="matchId">
				<form:options items="${matchDetails}" />
			</form:select>
			<br />
			<br />
			<input type="submit" value="Get Details" name="_getDetails" />
			<br />
			<br />
			<c:if test="${not empty selectedUsers}">
				<table>
					<tr>
						<c:if test="${not empty selectedUsers}">
							<td><form:select path="userIds" multiple="true">
									<form:options items="${selectedUsers}" />
								</form:select></td>
						</c:if>
					</tr>
				</table>
			</c:if>
		</form:form>
		<br />
	</tiles:putAttribute>
</tiles:insertDefinition>