<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Match Nomination</h2>

		<form:form id="nominationForm" method="post" action="match_nomination_submit"
			commandName="nominationBean">

			<form:select path="matchId">
				<form:options items="${matchDetails}" />
			</form:select>
			<br/><br/>
			<input type="submit" value="Nominate"/>
		</form:form>

	</tiles:putAttribute>
</tiles:insertDefinition>