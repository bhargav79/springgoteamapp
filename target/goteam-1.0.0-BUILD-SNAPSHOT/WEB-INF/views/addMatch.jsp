<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Add Match</h2>
		<br />
		<p>${matchStatus}</p>

		<form:form id="nominationForm" method="post" action="add_match_submit"
			commandName="matchBean">

			<form:errors path="*" />

			<form:label path="matchDetails">Enter Match Details</form:label>
			<form:input name="matchDetails" path="matchDetails" />
			<form:errors path="matchDetails" />
			<br>
			<form:label path="matchDate">Enter Match Schedule</form:label>
			<form:input name="matchDate" path="matchDate" />
			<form:errors path="matchDate" />
			<br>
			<br>
			<input type="submit" value="Submit"/>
		</form:form>

	</tiles:putAttribute>
</tiles:insertDefinition>