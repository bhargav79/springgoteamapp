package com.ms3.goteam.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ms3.goteam.model.TeamInfo;
import com.ms3.goteam.model.UserInfo;

public class TeamDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public TeamInfo getTeam(int teamId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		String hql = "FROM TeamInfo t WHERE t.id = :teamId";
		Query query = session.createQuery(hql);
		query.setParameter("teamId", teamId);
		List<TeamInfo> teamInfos = (List<TeamInfo>) query.list();
		session.close();
		if (!teamInfos.isEmpty() || teamInfos.size() != 0) {
			return (TeamInfo) teamInfos.get(0);
		} else
			return null;

	}

	public boolean persistTeam(TeamInfo teamInfo) {
		Session session = sessionFactory.openSession();
		try {

			Transaction transaction = session.beginTransaction();

			session.save(teamInfo);

			transaction.commit();
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			session.close();
		}

	}

	public List<TeamInfo> getTeamDetails() {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		String hql = "FROM TeamInfo t WHERE 1=1";
		Query query = session.createQuery(hql);
		List<TeamInfo> teamInfos = (List<TeamInfo>) query.list();
		session.close();
		return teamInfos;
	}

	public List<TeamInfo> getOpponentTeams(UserInfo userInfo) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		String hql = "FROM TeamInfo t WHERE t.id <> (SELECT u.teamInfo FROM UserInfo u WHERE u.id = :userId)";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userInfo.getId());
		List<TeamInfo> teamInfos = (List<TeamInfo>) query.list();
		session.close();
		return teamInfos;
	}

}
