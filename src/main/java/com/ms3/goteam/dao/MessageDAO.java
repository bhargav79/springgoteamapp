package com.ms3.goteam.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ms3.goteam.model.MessageInfo;
import com.ms3.goteam.model.TeamInfo;
import com.ms3.goteam.model.UserInfo;

public class MessageDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public boolean persistMessage(MessageInfo messageInfo) {
		Session session = sessionFactory.openSession();
		try {

			Transaction transaction = session.beginTransaction();

			session.save(messageInfo);

			transaction.commit();
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			session.close();
		}

	}
	
	public List<MessageInfo> getMessages(UserInfo userInfo) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		String hql = "FROM MessageInfo m WHERE m.teamInfo = (SELECT u.teamInfo FROM UserInfo u WHERE u.id = :userId)";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userInfo.getId());
		List<MessageInfo> teamInfos = (List<MessageInfo>) query.list();
		session.close();
		return teamInfos;
	}

}
