package com.ms3.goteam.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ms3.goteam.dto.MatchDTO;
import com.ms3.goteam.model.MatchInfo;
import com.ms3.goteam.model.TeamInfo;
import com.ms3.goteam.model.UserInfo;
import com.ms3.goteam.model.UserNomination;

public class MatchDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<MatchInfo> getAllMatchDetails(UserInfo userInfo) {

		Session session = sessionFactory.openSession();
		session.beginTransaction();

		String hql = "FROM MatchInfo m WHERE m.homeTeam = (SELECT u.teamInfo FROM UserInfo u WHERE u.id = :homeUserId) "
				+ "OR m.awayTeam = (SELECT u.teamInfo FROM UserInfo u WHERE u.id = :awayUserId)";
		Query query = session.createQuery(hql);
		query.setParameter("homeUserId", userInfo.getId());
		query.setParameter("awayUserId", userInfo.getId());
		List<MatchInfo> matchInfo = (List<MatchInfo>) query.list();
		session.close();
		return matchInfo;
	}

	public List<MatchInfo> getMatchDetails(UserInfo userInfo) {

		Session session = sessionFactory.openSession();
		session.beginTransaction();

		String hql = "FROM MatchInfo m WHERE "
				+ "m.homeTeam = (SELECT u.teamInfo FROM UserInfo u WHERE u.id = :homeUserId) "
				+ "OR m.awayTeam = (SELECT u.teamInfo FROM UserInfo u WHERE u.id = :awayUserId)";
		Query query = session.createQuery(hql);
		query.setParameter("homeUserId", userInfo.getId());
		query.setParameter("awayUserId", userInfo.getId());
		List<MatchInfo> matchInfo = (List<MatchInfo>) query.list();
		session.close();
		return matchInfo;
	}

	public boolean addMatch(MatchDTO matchDTO, UserInfo userInfo) {
		Session session = sessionFactory.openSession();

		try {
			Transaction transaction = session.beginTransaction();

			MatchInfo matchInfo = new MatchInfo();
			TeamInfo myTeam = null;
			TeamInfo opponentTeam = null;

			String myHql = "FROM TeamInfo t WHERE t.id = (SELECT u.teamInfo FROM UserInfo u WHERE u.id = :userId)";
			Query myQuery = session.createQuery(myHql);
			myQuery.setParameter("userId", userInfo.getId());
			List<TeamInfo> myTeamInfos = (List<TeamInfo>) myQuery.list();

			if (!myTeamInfos.isEmpty() || myTeamInfos.size() != 0) {
				myTeam = myTeamInfos.get(0);
			}

			String oppHql = "FROM TeamInfo t WHERE t.id = :teamId";
			Query oppQuery1 = session.createQuery(oppHql);
			oppQuery1.setParameter("teamId",
					Integer.parseInt(matchDTO.getTeamId()));
			List<TeamInfo> oppTeamInfos = (List<TeamInfo>) oppQuery1.list();

			if (!oppTeamInfos.isEmpty() || oppTeamInfos.size() != 0) {
				opponentTeam = oppTeamInfos.get(0);
			}

			if ("home".equals(matchDTO.getVenue())) {
				matchInfo.setHomeTeam(myTeam);
				matchInfo.setAwayTeam(opponentTeam);
				matchInfo.setLocation(myTeam.getLocation());
			} else if ("away".equals(matchDTO.getVenue())) {
				matchInfo.setAwayTeam(myTeam);
				matchInfo.setHomeTeam(opponentTeam);
				matchInfo.setLocation(opponentTeam.getLocation());
			} else {
				matchInfo.setHomeTeam(myTeam);
				matchInfo.setAwayTeam(opponentTeam);
				matchInfo.setLocation(matchDTO.getLocation());
			}

			SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
			System.out.println(sdf.parseObject(matchDTO.getSchedule()));
			matchInfo.setSchedule(matchDTO.getSchedule());
			session.save(matchInfo);

			transaction.commit();

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	public void nominateMatch(String matchId, UserInfo userInfo,
			boolean nomination) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		String checkHql = "FROM UserNomination u WHERE u.matchInfo = (SELECT id FROM MatchInfo m WHERE m.id = :matchId) "
				+ "AND u.userInfo = (SELECT id FROM UserInfo u WHERE u.id = :userId)";
		Query checkQuery = session.createQuery(checkHql);
		checkQuery.setParameter("matchId", Integer.parseInt(matchId));
		checkQuery.setParameter("userId", userInfo.getId());
		List<UserNomination> nominationList = (List<UserNomination>) checkQuery
				.list();

		if (null != nominationList
				&& (!nominationList.isEmpty() || nominationList.size() != 0)) {
			UserNomination userNomination = nominationList.get(0);

			userNomination.setNomination(nomination);
			session.save(userNomination);
		} else {

			String hql = "FROM MatchInfo m WHERE m.id = :matchId";
			Query query = session.createQuery(hql);
			query.setParameter("matchId", Integer.parseInt(matchId));
			List<MatchInfo> matchInfoList = (List<MatchInfo>) query.list();

			if (null != matchInfoList
					&& (!matchInfoList.isEmpty() || matchInfoList.size() != 0)) {
				MatchInfo matchInfo = matchInfoList.get(0);

				UserNomination userNomination = new UserNomination();
				userNomination.setMatchInfo(matchInfo);
				userNomination.setUserInfo(userInfo);
				userNomination.setNomination(nomination);
				session.save(userNomination);
			}
		}
		transaction.commit();
		session.close();
	}

	public List<UserInfo> getNominationDetails(String matchId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		String hql = "FROM UserInfo u WHERE u.id IN (SELECT n.userInfo FROM UserNomination n LEFT JOIN n.matchInfo m WHERE m.id = :matchId AND n.isSelected = 0 AND n.nomination = 1)";
		Query query = session.createQuery(hql);
		query.setParameter("matchId", Integer.parseInt(matchId));
		List<UserInfo> userInfos = (List<UserInfo>) query.list();
		session.close();
		return userInfos;
	}

	public List<UserInfo> getSelectedDetails(String matchId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		String hql = "FROM UserInfo u WHERE u.id IN (SELECT n.userInfo FROM UserNomination n LEFT JOIN n.matchInfo m WHERE m.id = :matchId AND n.isSelected = 1 AND n.nomination = 1)";
		Query query = session.createQuery(hql);
		query.setParameter("matchId", Integer.parseInt(matchId));
		List<UserInfo> userInfos = (List<UserInfo>) query.list();
		session.close();
		return userInfos;
	}

	public boolean setTeamSelection(List<String> userIds) {
		Session session = sessionFactory.openSession();

		try {
			Transaction transaction = session.beginTransaction();

			List<Integer> userIdsList = new ArrayList<Integer>();
			for (String userId : userIds) {
				userIdsList.add(Integer.parseInt(userId));
			}

			String hql = "UPDATE UserNomination n SET n.isSelected = 1 WHERE n.userInfo IN (FROM UserInfo u WHERE u.id IN (:userIdsList))";
			Query query = session.createQuery(hql);
			query.setParameterList("userIdsList", userIdsList);
			query.executeUpdate();

			transaction.commit();
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			session.close();
		}

	}

	public boolean setTeamDeSelection(List<String> userIds) {
		Session session = sessionFactory.openSession();

		try {
			Transaction transaction = session.beginTransaction();

			List<Integer> userIdsList = new ArrayList<Integer>();
			for (String userId : userIds) {
				userIdsList.add(Integer.parseInt(userId));
			}

			String hql = "UPDATE UserNomination n SET n.isSelected = 0 WHERE n.userInfo IN (FROM UserInfo u WHERE u.id IN (:userIdsList))";
			Query query = session.createQuery(hql);
			query.setParameterList("userIdsList", userIdsList);
			query.executeUpdate();

			transaction.commit();
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			session.close();
		}

	}

}
