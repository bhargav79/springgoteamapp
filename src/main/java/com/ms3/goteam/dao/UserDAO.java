package com.ms3.goteam.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ms3.goteam.model.TeamInfo;
import com.ms3.goteam.model.UserInfo;

public class UserDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<UserInfo> getUserDetails() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		String hql = "FROM UserInfo u WHERE u.isAdmin = 0 ";
		Query query = session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<UserInfo> userInfo = (List<UserInfo>) query.list();
		session.close();
		return userInfo;
	}

	public UserInfo validateUser(UserInfo userInfo) {

		Session session = sessionFactory.openSession();
		session.beginTransaction();

		String hql = "FROM UserInfo u WHERE u.userName = :userName "
				+ "and u.password = :password";
		Query query = session.createQuery(hql);
		query.setParameter("userName", userInfo.getUserName());
		query.setParameter("password", userInfo.getPassword());
		List results = query.list();
		session.close();
		if (!results.isEmpty() || results.size() != 0) {
			return (UserInfo) results.get(0);
		} else
			return null;

	}

	public boolean persistUser(UserInfo userInfo, String teamId) {
		Session session = sessionFactory.openSession();
		try {

			Transaction transaction = session.beginTransaction();

			String hql = "FROM TeamInfo u WHERE u.id = :teamId ";
			Query query = session.createQuery(hql);
			query.setParameter("teamId", Integer.parseInt(teamId));
			List results = query.list();
			TeamInfo teamInfo = null;
			if (!results.isEmpty() || results.size() != 0)
				teamInfo = (TeamInfo) results.get(0);

			userInfo.setTeamInfo(teamInfo);

			session.save(userInfo);

			transaction.commit();
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			session.close();
		}

	}

	public boolean checkUserName(String userName) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		String hql = "FROM UserInfo u WHERE u.userName = :userName ";
		Query query = session.createQuery(hql);
		query.setParameter("userName", userName);

		List results = query.list();
		session.close();
		if (!results.isEmpty() || results.size() != 0)
			return false;
		else
			return true;
	}

	public boolean makeAdmin(int id) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		String hql = "FROM UserInfo u WHERE u.id = :id ";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);

		List results = query.list();

		if (!results.isEmpty() || results.size() != 0) {

			UserInfo userInfo = (UserInfo) results.get(0);
			userInfo.setAdmin(true);
			session.save(userInfo);
			transaction.commit();
			session.close();
			return true;
		}

		else {
			session.close();
			return false;
		}
	}

}
