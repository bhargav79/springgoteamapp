package com.ms3.goteam.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "user_info")
public class UserInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private int id;

	@Column
	private String firstName;

	@Column
	private String lastName;

	@Column(unique = true)
	private String userName;

	@Column
	private String password;

	@Column
	private String dob;

	@Column(unique = true)
	private String emailId;

	@Column
	private boolean isAdmin = false;

	@ManyToOne
	@JoinColumn(name = "team_id")
	private TeamInfo teamInfo;

	@OneToMany
	private Set<UserNomination> userNominations = new HashSet<UserNomination>();

	@OneToMany
	private Set<MessageInfo> messageInfos = new HashSet<MessageInfo>();

	public Set<UserNomination> getUserNominations() {
		return userNominations;
	}

	public void setUserNominations(Set<UserNomination> userNominations) {
		this.userNominations = userNominations;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public TeamInfo getTeamInfo() {
		return teamInfo;
	}

	public void setTeamInfo(TeamInfo teamInfo) {
		this.teamInfo = teamInfo;
	}

	public Set<MessageInfo> getMessageInfos() {
		return messageInfos;
	}

	public void setMessageInfos(Set<MessageInfo> messageInfos) {
		this.messageInfos = messageInfos;
	}

}
