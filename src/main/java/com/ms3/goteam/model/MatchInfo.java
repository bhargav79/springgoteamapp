package com.ms3.goteam.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "match_info")
public class MatchInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "match_id")
	private int id;

	@ManyToOne
	@JoinColumn(name = "home_team")
	private TeamInfo homeTeam;

	@ManyToOne
	@JoinColumn(name = "away_team")
	private TeamInfo awayTeam;

	@Column
	private String location;

	@Column
	/*@DateTimeFormat(pattern = "mmYYYY")
	@Temporal(TemporalType.DATE)*/
	private String schedule;

	@OneToMany
	private Set<UserNomination> userNominations = new HashSet<UserNomination>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TeamInfo getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(TeamInfo homeTeam) {
		this.homeTeam = homeTeam;
	}

	public TeamInfo getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(TeamInfo awayTeam) {
		this.awayTeam = awayTeam;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}


	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public Set<UserNomination> getUserNominations() {
		return userNominations;
	}

	public void setUserNominations(Set<UserNomination> userNominations) {
		this.userNominations = userNominations;
	}

}
