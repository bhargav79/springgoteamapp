package com.ms3.goteam.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "team_info")
public class TeamInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "team_id")
	private int id;

	@Column
	private String teamName;

	@Column
	private String teamCode;

	@Column
	private String location;

	@OneToMany
	private Set<UserInfo> userInfo = new HashSet<UserInfo>();

	@OneToMany
	private Set<MatchInfo> matchInfos = new HashSet<MatchInfo>();

	@OneToMany
	private Set<MessageInfo> messageInfos = new HashSet<MessageInfo>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(String teamCode) {
		this.teamCode = teamCode;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Set<UserInfo> getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(Set<UserInfo> userInfo) {
		this.userInfo = userInfo;
	}

	public Set<MatchInfo> getMatchInfos() {
		return matchInfos;
	}

	public void setMatchInfos(Set<MatchInfo> matchInfos) {
		this.matchInfos = matchInfos;
	}

	public Set<MessageInfo> getMessageInfos() {
		return messageInfos;
	}

	public void setMessageInfos(Set<MessageInfo> messageInfos) {
		this.messageInfos = messageInfos;
	}

}
