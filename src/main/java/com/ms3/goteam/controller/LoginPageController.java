package com.ms3.goteam.controller;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ms3.goteam.dto.HomeDTO;
import com.ms3.goteam.dto.NominationDTO;
import com.ms3.goteam.dto.UserLoginDTO;
import com.ms3.goteam.dto.UserRegisterDTO;
import com.ms3.goteam.model.MatchInfo;
import com.ms3.goteam.model.MessageInfo;
import com.ms3.goteam.model.TeamInfo;
import com.ms3.goteam.model.UserInfo;
import com.ms3.goteam.service.MatchService;
import com.ms3.goteam.service.MessageService;
import com.ms3.goteam.service.TeamService;
import com.ms3.goteam.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/login")
public class LoginPageController {

	private static final Logger logger = LoggerFactory
			.getLogger(LoginPageController.class);

	@Autowired
	UserService userService;

	@Autowired
	MatchService matchService;

	@Autowired
	TeamService teamService;
	
	@Autowired
	MessageService messageService;

	@RequestMapping(method = RequestMethod.POST, params = "_login")
	public ModelAndView displayLogin(@Valid UserLoginDTO userLogin,
			BindingResult result, HttpSession session) {

		UserInfo userDetails = userService.validateCredentials(userLogin);

		if (null == userDetails) {
			ModelAndView modelAndView = new ModelAndView("loginPage");
			modelAndView.addObject("loginStatus", "Invalid Credentials");
			modelAndView.addObject("userLogin", userLogin);
			return modelAndView;
		} else {
			session.setAttribute("userDetails", userDetails);

			ModelAndView modelAndView = new ModelAndView("home");
			List<HomeDTO> homeDTOs = matchService.getAllMatchDetails(userDetails);
			modelAndView.addObject("homeDTOs", homeDTOs);
			List<MessageInfo> messageInfos = messageService.getMessages(userDetails);
			modelAndView.addObject("messageInfos", messageInfos);
			modelAndView.addObject("nominationBean", new NominationDTO());
			modelAndView.addObject("adminStatus", userDetails.isAdmin());
			return modelAndView;
		}
	}

	@RequestMapping(method = RequestMethod.POST, params = "_register")
	public ModelAndView displayRegister(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("registerPage");
		UserRegisterDTO userRegisterDTO = new UserRegisterDTO();
		modelAndView.addObject("userRegister", userRegisterDTO);

		List<TeamInfo> teamInfoList = teamService.getTeamDetails();

		Map<String, String> teamMap = new LinkedHashMap<String, String>();
		for (Iterator<TeamInfo> iterator = teamInfoList.iterator(); iterator
				.hasNext();) {
			TeamInfo teamInfo = (TeamInfo) iterator.next();
			teamMap.put(String.valueOf(teamInfo.getId()),
					teamInfo.getTeamCode() + " - " +teamInfo.getTeamName());
		}
		modelAndView.addObject("teamMap", teamMap);

		return modelAndView;
	}

}
