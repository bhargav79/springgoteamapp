package com.ms3.goteam.controller;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.ms3.goteam.dto.HomeDTO;
import com.ms3.goteam.dto.MatchDTO;
import com.ms3.goteam.dto.MessageDTO;
import com.ms3.goteam.dto.NominationDTO;
import com.ms3.goteam.dto.TeamDTO;
import com.ms3.goteam.dto.UserDTO;
import com.ms3.goteam.model.MatchInfo;
import com.ms3.goteam.model.MessageInfo;
import com.ms3.goteam.model.TeamInfo;
import com.ms3.goteam.model.UserInfo;
import com.ms3.goteam.service.MatchService;
import com.ms3.goteam.service.MessageService;
import com.ms3.goteam.service.TeamService;
import com.ms3.goteam.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/menu")
@SessionAttributes("userDetails")
public class MenuPageController {

	@Autowired
	MatchService matchService;

	@Autowired
	UserService userService;

	@Autowired
	TeamService teamService;

	@Autowired
	MessageService messageService;

	private static final Logger logger = LoggerFactory
			.getLogger(MenuPageController.class);

	@RequestMapping(value = "/homepage")
	public ModelAndView getHomePage(
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("home");
		List<HomeDTO> homeDTOs = matchService.getAllMatchDetails(userSession);
		modelAndView.addObject("homeDTOs", homeDTOs);
		List<MessageInfo> messageInfos = messageService.getMessages(userSession);
		modelAndView.addObject("messageInfos", messageInfos);
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/match_nomination")
	public ModelAndView nominateMatch(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("matchNomination");
		List<HomeDTO> homeDTOs = matchService.getMatchDetails(userSession);

		Map<String, String> matchDetails = new LinkedHashMap<String, String>();
		for (HomeDTO homeDTO : homeDTOs) {
			matchDetails.put(String.valueOf(homeDTO.getMatchId()),
					homeDTO.getAwayTeam() + " @ " + homeDTO.getHomeTeam());
		}

		modelAndView.addObject("matchDetails", matchDetails);
		modelAndView.addObject("nominationBean", new NominationDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/match_nomination_submit", params = "_nominate")
	public ModelAndView nominateMatchSubmit(@Valid NominationDTO nominationDTO,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("matchNomination");

		List<HomeDTO> homeDTOs = matchService.getMatchDetails(userSession);

		Map<String, String> matchDetails = new LinkedHashMap<String, String>();
		for (HomeDTO homeDTO : homeDTOs) {
			matchDetails.put(String.valueOf(homeDTO.getMatchId()),
					homeDTO.getAwayTeam() + " @ " + homeDTO.getHomeTeam());
		}

		if (null == nominationDTO.getNomination()
				|| nominationDTO.getNomination().isEmpty()) {
			modelAndView.addObject("matchDetails", matchDetails);
			modelAndView.addObject("nominationBean", new NominationDTO());
			modelAndView.addObject("adminStatus", userSession.isAdmin());
			modelAndView.addObject("nominationStatus",
					"Please select Yes or No");
			return modelAndView;
		}

		matchService.nominateMatch(nominationDTO.getMatchId(), userSession,
				nominationDTO.getNomination());

		modelAndView.addObject("matchDetails", matchDetails);
		modelAndView.addObject("nominationBean", new NominationDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/match_nomination_submit", params = "_getdetails")
	public ModelAndView nominateMatchGetDetails(
			@Valid NominationDTO nominationDTO,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("matchNomination");

		List<UserInfo> selectedUsers = matchService
				.getMatchNominations(nominationDTO.getMatchId());

		List<HomeDTO> homeDTOs = matchService.getMatchDetails(userSession);

		Map<String, String> matchDetails = new LinkedHashMap<String, String>();
		for (HomeDTO homeDTO : homeDTOs) {
			matchDetails.put(String.valueOf(homeDTO.getMatchId()),
					homeDTO.getAwayTeam() + " @ " + homeDTO.getHomeTeam());
		}

		Map<String, String> selectedUsersMap = new LinkedHashMap<String, String>();
		for (Iterator<UserInfo> iterator = selectedUsers.iterator(); iterator
				.hasNext();) {
			UserInfo userInfo = (UserInfo) iterator.next();
			selectedUsersMap.put(String.valueOf(userInfo.getId()),
					userInfo.getFirstName() + " " + userInfo.getLastName());
		}

		modelAndView.addObject("matchDetails", matchDetails);
		modelAndView.addObject("nominationBean", new NominationDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		modelAndView.addObject("selectedUsers", selectedUsersMap);
		return modelAndView;
	}

	@RequestMapping(value = "/team_details")
	public ModelAndView getTeamDetails(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("teamDetails");
		List<HomeDTO> homeDTOs = matchService.getAllMatchDetails(userSession);

		Map<String, String> matchDetails = new LinkedHashMap<String, String>();
		for (HomeDTO homeDTO : homeDTOs) {
			matchDetails.put(String.valueOf(homeDTO.getMatchId()),
					homeDTO.getAwayTeam() + " @ " + homeDTO.getHomeTeam());
		}

		modelAndView.addObject("matchDetails", matchDetails);
		modelAndView.addObject("nominationBean", new NominationDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/team_details_submit")
	public ModelAndView teamDetailsSubmit(@Valid NominationDTO nominationDTO,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("teamDetails");

		List<UserInfo> selectedUsers = matchService
				.getSelectedDetails(nominationDTO.getMatchId());
		List<HomeDTO> homeDTOs = matchService.getAllMatchDetails(userSession);

		Map<String, String> matchDetails = new LinkedHashMap<String, String>();
		for (HomeDTO homeDTO : homeDTOs) {
			matchDetails.put(String.valueOf(homeDTO.getMatchId()),
					homeDTO.getAwayTeam() + " @ " + homeDTO.getHomeTeam());
		}

		Map<String, String> selectedUsersMap = new LinkedHashMap<String, String>();
		for (Iterator<UserInfo> iterator = selectedUsers.iterator(); iterator
				.hasNext();) {
			UserInfo userInfo = (UserInfo) iterator.next();
			selectedUsersMap.put(String.valueOf(userInfo.getId()),
					userInfo.getFirstName() + " " + userInfo.getLastName());
		}

		modelAndView.addObject("matchDetails", matchDetails);
		modelAndView.addObject("nominationBean", new NominationDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		modelAndView.addObject("selectedUsers", selectedUsersMap);
		return modelAndView;
	}

	@RequestMapping(value = "/add_match")
	public ModelAndView addMatch(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("addMatch");
		modelAndView.addObject("matchBean", new MatchDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());

		List<TeamInfo> teamInfoList = teamService.getOpponentTeams(userSession);

		Map<String, String> teamMap = new LinkedHashMap<String, String>();
		for (Iterator<TeamInfo> iterator = teamInfoList.iterator(); iterator
				.hasNext();) {
			TeamInfo teamInfo = (TeamInfo) iterator.next();
			teamMap.put(String.valueOf(teamInfo.getId()),
					teamInfo.getTeamCode() + " - " + teamInfo.getTeamName());
		}
		modelAndView.addObject("teamMap", teamMap);

		Map<String, String> venueMap = new LinkedHashMap<String, String>();
		venueMap.put("home", "Home");
		venueMap.put("away", "Away");
		venueMap.put("neutral", "Neutral");
		modelAndView.addObject("venueMap", venueMap);

		return modelAndView;
	}

	@RequestMapping(value = "/add_match_submit", method = RequestMethod.POST)
	public ModelAndView addMatchSubmit(@Valid MatchDTO matchDTO,
			BindingResult result,
			@ModelAttribute("userDetails") UserInfo userSession) {

		ModelAndView modelAndView = new ModelAndView("addMatch");
		modelAndView.addObject("adminStatus", userSession.isAdmin());

		List<TeamInfo> teamInfoList = teamService.getOpponentTeams(userSession);

		Map<String, String> teamMap = new LinkedHashMap<String, String>();
		for (Iterator<TeamInfo> iterator = teamInfoList.iterator(); iterator
				.hasNext();) {
			TeamInfo teamInfo = (TeamInfo) iterator.next();
			teamMap.put(String.valueOf(teamInfo.getId()),
					teamInfo.getTeamCode() + " - " + teamInfo.getTeamName());
		}
		modelAndView.addObject("teamMap", teamMap);

		Map<String, String> venueMap = new LinkedHashMap<String, String>();
		venueMap.put("home", "Home");
		venueMap.put("away", "Away");
		venueMap.put("neutral", "Neutral");
		modelAndView.addObject("venueMap", venueMap);

		if (isNullOrEmpty(matchDTO.getTeamId())
				|| isNullOrEmpty(matchDTO.getVenue())
				|| isNullOrEmpty(matchDTO.getSchedule())) {
			modelAndView.addObject("matchBean", matchDTO);
			modelAndView.addObject("matchStatus",
					"Please enter mandatory fields");
			return modelAndView;
		}

		if ("neutral".equals(matchDTO.getVenue())) {
			if (isNullOrEmpty(matchDTO.getLocation())) {
				modelAndView.addObject("matchBean", matchDTO);
				modelAndView.addObject("matchStatus", "Please enter location");
				return modelAndView;
			}
		}

		matchService.addMatch(matchDTO, userSession);

		modelAndView.addObject("matchStatus", "Match added succesfully");
		modelAndView.addObject("matchBean", matchDTO);

		return modelAndView;

	}

	@RequestMapping(value = "/add_message")
	public ModelAndView addMessage(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("addMessage");
		modelAndView.addObject("messageBean", new MessageDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/add_message_submit", method = RequestMethod.POST)
	public ModelAndView addMessageSubmit(@Valid MessageDTO messageDTO,
			BindingResult result,
			@ModelAttribute("userDetails") UserInfo userSession) {

		ModelAndView modelAndView = new ModelAndView("addMessage");

		if (isNullOrEmpty(messageDTO.getMessage())) {
			modelAndView.addObject("matchStatus", "Please enter the message");
		} else {
			messageService.addMessage(userSession, messageDTO.getMessage());
			modelAndView.addObject("matchStatus", "Message added successfully");
		}

		modelAndView.addObject("messageBean", new MessageDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/team_selection")
	public ModelAndView teamSelection(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("teamSelection");
		List<HomeDTO> homeDTOs = matchService.getAllMatchDetails(userSession);

		Map<String, String> matchDetails = new LinkedHashMap<String, String>();
		for (HomeDTO homeDTO : homeDTOs) {
			matchDetails.put(String.valueOf(homeDTO.getMatchId()),
					homeDTO.getAwayTeam() + " @ " + homeDTO.getHomeTeam());
		}

		modelAndView.addObject("matchDetails", matchDetails);
		modelAndView.addObject("nominationBean", new NominationDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/team_selection_submit", params = "_getDetails")
	public ModelAndView teamSelectionGetDetails(
			@Valid NominationDTO nominationDTO,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("teamSelection");

		List<UserInfo> userInfos = matchService
				.getMatchNominations(nominationDTO.getMatchId());
		List<UserInfo> selectedUsers = matchService
				.getSelectedDetails(nominationDTO.getMatchId());
		List<HomeDTO> homeDTOs = matchService.getAllMatchDetails(userSession);

		Map<String, String> matchDetails = new LinkedHashMap<String, String>();
		for (HomeDTO homeDTO : homeDTOs) {
			matchDetails.put(String.valueOf(homeDTO.getMatchId()),
					homeDTO.getAwayTeam() + " @ " + homeDTO.getHomeTeam());
		}

		Map<String, String> nominatedUsers = new LinkedHashMap<String, String>();
		for (Iterator<UserInfo> iterator = userInfos.iterator(); iterator
				.hasNext();) {
			UserInfo userInfo = (UserInfo) iterator.next();
			nominatedUsers.put(String.valueOf(userInfo.getId()),
					userInfo.getFirstName() + " " + userInfo.getLastName());
		}

		Map<String, String> selectedUsersMap = new LinkedHashMap<String, String>();
		for (Iterator<UserInfo> iterator = selectedUsers.iterator(); iterator
				.hasNext();) {
			UserInfo userInfo = (UserInfo) iterator.next();
			selectedUsersMap.put(String.valueOf(userInfo.getId()),
					userInfo.getFirstName() + " " + userInfo.getLastName());
		}

		modelAndView.addObject("matchDetails", matchDetails);
		modelAndView.addObject("nominationBean", new NominationDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		modelAndView.addObject("nominatedUsers", nominatedUsers);
		modelAndView.addObject("selectedUsers", selectedUsersMap);
		return modelAndView;
	}

	@RequestMapping(value = "/team_selection_submit", params = "_selection")
	public ModelAndView teamSelectionSubmit(@Valid NominationDTO nominationDTO,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("teamSelection");

		boolean selectionStatus = matchService.setTeamSelection(nominationDTO
				.getUserIds());
		List<HomeDTO> homeDTOs = matchService.getAllMatchDetails(userSession);

		Map<String, String> matchDetails = new LinkedHashMap<String, String>();
		for (HomeDTO homeDTO : homeDTOs) {
			matchDetails.put(String.valueOf(homeDTO.getMatchId()),
					homeDTO.getAwayTeam() + " @ " + homeDTO.getHomeTeam());
		}

		if (selectionStatus)
			modelAndView.addObject("selectionStatus", "Selection Done!!!");
		else
			modelAndView.addObject("selectionStatus",
					"Selection wasn't successful");

		modelAndView.addObject("matchDetails", matchDetails);
		modelAndView.addObject("nominationBean", new NominationDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/team_selection_submit", params = "_deselection")
	public ModelAndView teamDeSelectionSubmit(
			@Valid NominationDTO nominationDTO,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("teamSelection");

		boolean selectionStatus = matchService.setTeamDeSelection(nominationDTO
				.getUserIds());
		List<HomeDTO> homeDTOs = matchService.getAllMatchDetails(userSession);

		Map<String, String> matchDetails = new LinkedHashMap<String, String>();
		for (HomeDTO homeDTO : homeDTOs) {
			matchDetails.put(String.valueOf(homeDTO.getMatchId()),
					homeDTO.getAwayTeam() + " @ " + homeDTO.getHomeTeam());
		}

		if (selectionStatus)
			modelAndView.addObject("selectionStatus", "DeSelection Done!!!");
		else
			modelAndView.addObject("selectionStatus",
					"DeSelection wasn't successful");

		modelAndView.addObject("matchDetails", matchDetails);
		modelAndView.addObject("nominationBean", new NominationDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/add_team")
	public ModelAndView addTeam(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("addTeam");
		modelAndView.addObject("teamBean", new TeamDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/add_team_submit", method = RequestMethod.POST)
	public ModelAndView addTeamSubmit(@Valid TeamDTO teamDTO,
			BindingResult result,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("addTeam");
		modelAndView.addObject("adminStatus", userSession.isAdmin());

		if (isNullOrEmpty(teamDTO.getTeamName())
				|| isNullOrEmpty(teamDTO.getTeamCode())
				|| isNullOrEmpty(teamDTO.getLocation())) {
			modelAndView.addObject("teamBean", teamDTO);
			modelAndView.addObject("teamStatus",
					"Please enter mandatory fields");
			return modelAndView;
		}

		boolean teamStatus = teamService.addTeam(teamDTO);

		if (teamStatus) {
			modelAndView.addObject("teamBean", new TeamDTO());
			modelAndView.addObject("teamStatus", "Team added succesfully");
		} else {
			modelAndView.addObject("teamBean", teamDTO);
			modelAndView.addObject("teamStatus", "Team couldn't be added");
		}

		return modelAndView;

	}

	@RequestMapping(value = "/make_admin")
	public ModelAndView makeAdmin(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("userDetails") UserInfo session,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("makeAdmin");
		List<UserInfo> userInfos = userService.getUserDetails();
		System.out.println(session.getEmailId());
		Map<String, String> userDetails = new LinkedHashMap<String, String>();
		for (Iterator<UserInfo> iterator = userInfos.iterator(); iterator
				.hasNext();) {
			UserInfo userInfo = (UserInfo) iterator.next();
			userDetails.put(String.valueOf(userInfo.getId()),
					userInfo.getFirstName() + " " + userInfo.getLastName());
		}

		modelAndView.addObject("userDetails", userDetails);
		modelAndView.addObject("userBean", new UserDTO());
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;
	}

	@RequestMapping(value = "/make_admin_submit", method = RequestMethod.POST)
	public ModelAndView makeAdminSubmit(@Valid UserDTO userDTO,
			BindingResult result,
			@ModelAttribute("userDetails") UserInfo userSession) {
		ModelAndView modelAndView = new ModelAndView("makeAdmin");
		if (isNullOrEmpty(userDTO.getUserId())) {
			modelAndView.addObject("adminAccessStatus",
					"Please select the user");
		} else {

			boolean adminStatus = userService.makeAdmin(userDTO);

			if (adminStatus)
				modelAndView
						.addObject("adminAccessStatus", "Admin succesfully");
			else
				modelAndView.addObject("adminAccessStatus",
						"Admin unsuccesfully");
		}

		List<UserInfo> userInfos = userService.getUserDetails();

		Map<String, String> userDetails = new LinkedHashMap<String, String>();
		for (Iterator<UserInfo> iterator = userInfos.iterator(); iterator
				.hasNext();) {
			UserInfo userInfo = (UserInfo) iterator.next();
			userDetails.put(String.valueOf(userInfo.getId()),
					userInfo.getFirstName() + " " + userInfo.getLastName());
		}

		modelAndView.addObject("userDetails", userDetails);
		modelAndView.addObject("userBean", userDTO);
		modelAndView.addObject("adminStatus", userSession.isAdmin());
		return modelAndView;

	}

	@RequestMapping(value = "/logout")
	public String logout(SessionStatus status, HttpServletRequest request) {

		status.setComplete();
		request.getSession().invalidate();
		return "redirect:/";

	}

	private boolean isNullOrEmpty(Object object) {

		if (null == object)
			return true;

		if (object instanceof String) {
			if (null == object || ((String) object).isEmpty())
				return true;
		}
		return false;
	}

}
