package com.ms3.goteam.controller;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ms3.goteam.dto.UserLoginDTO;
import com.ms3.goteam.dto.UserRegisterDTO;
import com.ms3.goteam.model.TeamInfo;
import com.ms3.goteam.service.TeamService;
import com.ms3.goteam.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/register")
public class RegisterPageController {

	@Autowired
	UserService userService;

	@Autowired
	TeamService teamService;

	private static final Logger logger = LoggerFactory
			.getLogger(RegisterPageController.class);

	@RequestMapping(method = RequestMethod.POST, params = "_back")
	public ModelAndView displayLogin(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("loginPage");
		UserLoginDTO loginDTO = new UserLoginDTO();
		modelAndView.addObject("userLogin", loginDTO);

		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST, params = "_register")
	public ModelAndView registerUser(HttpServletRequest request,
			HttpServletResponse response, @Valid UserRegisterDTO userRegister,
			BindingResult result) {
		ModelAndView modelAndView = new ModelAndView("registerPage");
		modelAndView.addObject("userRegister", userRegister);
		if (result.hasErrors())
			return modelAndView;

		boolean registerStatus = userService.saveRegistration(userRegister);

		if (registerStatus) {
			ModelAndView loginView = new ModelAndView("loginPage");
			loginView.addObject("userLogin", new UserLoginDTO());
			loginView.addObject("loginStatus", "Registration Successful");
			return loginView;
		} else {

			modelAndView.addObject("registerStatus",
					"Existing username, please use other username to register");

		}
		List<TeamInfo> teamInfoList = teamService.getTeamDetails();

		Map<String, String> teamMap = new LinkedHashMap<String, String>();
		for (Iterator<TeamInfo> iterator = teamInfoList.iterator(); iterator
				.hasNext();) {
			TeamInfo teamInfo = (TeamInfo) iterator.next();
			teamMap.put(String.valueOf(teamInfo.getId()),
					teamInfo.getTeamCode() + " - " + teamInfo.getTeamName());
		}
		modelAndView.addObject("teamMap", teamMap);

		return modelAndView;

	}

	@RequestMapping(method = RequestMethod.POST, params = "_checkUser")
	public ModelAndView checkUserName(HttpServletRequest request,
			HttpServletResponse response, @Valid UserRegisterDTO userRegister,
			BindingResult result) {
		ModelAndView modelAndView = new ModelAndView("registerPage");
		modelAndView.addObject("userRegister", userRegister);

		boolean userNameStatus = userService.checkUserName(userRegister
				.getUserName());

		if (userNameStatus)
			modelAndView.addObject("registerStatus", "Valid User Name");
		else
			modelAndView.addObject("registerStatus",
					"Existing username, please use other username to register");

		List<TeamInfo> teamInfoList = teamService.getTeamDetails();

		Map<String, String> teamMap = new LinkedHashMap<String, String>();
		for (Iterator<TeamInfo> iterator = teamInfoList.iterator(); iterator
				.hasNext();) {
			TeamInfo teamInfo = (TeamInfo) iterator.next();
			teamMap.put(String.valueOf(teamInfo.getId()),
					teamInfo.getTeamCode() + " - " + teamInfo.getTeamName());
		}
		modelAndView.addObject("teamMap", teamMap);

		return modelAndView;
	}

}
