package com.ms3.goteam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms3.goteam.dao.MessageDAO;
import com.ms3.goteam.model.MessageInfo;
import com.ms3.goteam.model.UserInfo;

@Service
public class MessageService {
	
	@Autowired
	MessageDAO messageDAO;
	
	public boolean addMessage(UserInfo userInfo, String message){
		
		MessageInfo messageInfo = new MessageInfo();
		messageInfo.setUserInfo(userInfo);
		messageInfo.setMessage(message);
		messageInfo.setTeamInfo(userInfo.getTeamInfo());
		return messageDAO.persistMessage(messageInfo);
		
	}
	
	public List<MessageInfo> getMessages(UserInfo userInfo) {
		return messageDAO.getMessages(userInfo);
	}

}
