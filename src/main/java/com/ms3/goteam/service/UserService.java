package com.ms3.goteam.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms3.goteam.dao.UserDAO;
import com.ms3.goteam.dto.UserDTO;
import com.ms3.goteam.dto.UserLoginDTO;
import com.ms3.goteam.dto.UserRegisterDTO;
import com.ms3.goteam.model.MatchInfo;
import com.ms3.goteam.model.UserInfo;

@Service
public class UserService {

	@Autowired
	UserDAO userDAO;

	public UserInfo validateCredentials(UserLoginDTO userLogin) {
		UserInfo userInfo = new UserInfo();
		userInfo.setUserName(userLogin.getUserName());
		userInfo.setPassword(userLogin.getPassword());

		return userDAO.validateUser(userInfo);
	}

	public boolean saveRegistration(UserRegisterDTO userRegister) {

		UserInfo userInfo = new UserInfo();
		userInfo.setFirstName(userRegister.getFirstName());
		userInfo.setLastName(userRegister.getLastName());
		userInfo.setUserName(userRegister.getUserName());
		userInfo.setPassword(userRegister.getPassword());
		userInfo.setDob(userRegister.getDob());
		userInfo.setEmailId(userRegister.getEmailId());

		return userDAO.persistUser(userInfo, userRegister.getTeamId());
	}

	public boolean checkUserName(String userName) {
		return userDAO.checkUserName(userName);
	}

	public List<UserInfo> getUserDetails() {
		return userDAO.getUserDetails();
	}
	
	public boolean makeAdmin(UserDTO userDTO) {
		return userDAO.makeAdmin(Integer.parseInt(userDTO.getUserId()));
	}

}
