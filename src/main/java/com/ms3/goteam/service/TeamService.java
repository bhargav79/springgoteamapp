package com.ms3.goteam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms3.goteam.dao.TeamDAO;
import com.ms3.goteam.dto.TeamDTO;
import com.ms3.goteam.model.TeamInfo;
import com.ms3.goteam.model.UserInfo;

@Service
public class TeamService {

	@Autowired
	TeamDAO teamDAO;

	public List<TeamInfo> getTeamDetails() {
		return teamDAO.getTeamDetails();
	}
	
	public boolean addTeam(TeamDTO teamDTO) {
		TeamInfo teamInfo = new TeamInfo();
		teamInfo.setTeamName(teamDTO.getTeamName());
		teamInfo.setLocation(teamDTO.getLocation());
		teamInfo.setTeamCode(teamDTO.getTeamCode());
		return teamDAO.persistTeam(teamInfo);
	}
	
	public List<TeamInfo> getOpponentTeams(UserInfo userInfo){
		return teamDAO.getOpponentTeams(userInfo);
	}

}
