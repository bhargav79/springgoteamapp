package com.ms3.goteam.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms3.goteam.dao.MatchDAO;
import com.ms3.goteam.dao.TeamDAO;
import com.ms3.goteam.dto.HomeDTO;
import com.ms3.goteam.dto.MatchDTO;
import com.ms3.goteam.model.MatchInfo;
import com.ms3.goteam.model.TeamInfo;
import com.ms3.goteam.model.UserInfo;

@Service
public class MatchService {

	@Autowired
	MatchDAO matchDAO;

	@Autowired
	TeamDAO teamDAO;

	public List<HomeDTO> getAllMatchDetails(UserInfo userInfo) {
		List<MatchInfo> matchInfos = matchDAO.getAllMatchDetails(userInfo);

		List<HomeDTO> homeDTOs = new ArrayList<HomeDTO>();
		int count = 1;
		for (MatchInfo matchInfo : matchInfos) {
			HomeDTO homeDTO = new HomeDTO();
			TeamInfo homeTeam = teamDAO
					.getTeam(matchInfo.getHomeTeam().getId());
			homeDTO.setHomeTeam(homeTeam.getTeamName());
			TeamInfo awayTeam = teamDAO
					.getTeam(matchInfo.getAwayTeam().getId());
			homeDTO.setAwayTeam(awayTeam.getTeamName());
			homeDTO.setSchedule(matchInfo.getSchedule());
			homeDTO.setMatchId(String.valueOf(matchInfo.getId()));
			homeDTO.setId(String.valueOf(count));
			count++;
			homeDTOs.add(homeDTO);
		}
		return homeDTOs;
	}

	public List<HomeDTO> getMatchDetails(UserInfo userInfo) {
		List<MatchInfo> matchInfos = matchDAO.getMatchDetails(userInfo);

		List<HomeDTO> homeDTOs = new ArrayList<HomeDTO>();
		int count = 1;
		for (MatchInfo matchInfo : matchInfos) {
			HomeDTO homeDTO = new HomeDTO();
			TeamInfo homeTeam = teamDAO
					.getTeam(matchInfo.getHomeTeam().getId());
			homeDTO.setHomeTeam(homeTeam.getTeamName());
			TeamInfo awayTeam = teamDAO
					.getTeam(matchInfo.getAwayTeam().getId());
			homeDTO.setAwayTeam(awayTeam.getTeamName());
			homeDTO.setSchedule(matchInfo.getSchedule());
			homeDTO.setMatchId(String.valueOf(matchInfo.getId()));
			homeDTOs.add(homeDTO);
		}
		return homeDTOs;
	}

	public boolean addMatch(MatchDTO matchDTO, UserInfo userInfo) {

		return matchDAO.addMatch(matchDTO, userInfo);
	}

	public void nominateMatch(String matchId, UserInfo userInfo,
			String nomination) {
		boolean nominationValue = false;

		if ("yes".equalsIgnoreCase(nomination))
			nominationValue = true;

		matchDAO.nominateMatch(matchId, userInfo, nominationValue);
	}

	public List<UserInfo> getMatchNominations(String matchId) {
		return matchDAO.getNominationDetails(matchId);
	}

	public List<UserInfo> getSelectedDetails(String matchId) {
		return matchDAO.getSelectedDetails(matchId);
	}

	public boolean setTeamSelection(List<String> userIds) {
		return matchDAO.setTeamSelection(userIds);
	}

	public boolean setTeamDeSelection(List<String> userIds) {
		return matchDAO.setTeamDeSelection(userIds);
	}

}
