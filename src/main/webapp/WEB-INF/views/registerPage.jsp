<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<title>Go Team</title>
<head>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  
  <script>
  $(document).ready(function() {
    $("#dob").datepicker();
  });
  </script>
</head>
<body>
	<h1 align="center">Go Team Registration</h1>
	<br />
	<p>${registerStatus}</p>

	<form:form id="registrationForm" method="post" action="register"
		commandName="userRegister">

		<form:errors path="*" />

		<form:label path="firstName">Enter your First Name</form:label>
		<form:input name="firstName" path="firstName" />
		<form:errors path="firstName" />
		<br>
		<form:label path="lastName">Please enter Last Name</form:label>
		<form:input name="lastName" path="lastName" />
		<form:errors path="lastName" />
		<br>
		<form:label path="dob">Please enter Date of Birth</form:label>
		<form:input name="dob" path="dob" id="dob" />
		<form:errors path="dob" />
		<br>
		<form:label path="userName">Enter your User Name</form:label>
		<form:input name="userName" path="userName" />
		<form:errors path="userName" />
		<input type="submit" value="Check Username" name="_checkUser" />
		<br>
		<form:label path="password">Please enter your password</form:label>
		<form:password name="password" path="password" />
		<form:errors path="password" />
		<br>
		<form:label path="emailId">Please enter your email id</form:label>
		<form:input name="emailId" path="emailId" />
		<form:errors path="emailId" />
		<br>
		<c:if test="${not empty teamMap}">
			<form:label path="teamId">Please select your team</form:label>
			<form:select path="teamId">
				<form:option value="" >....</form:option>
				<form:options items="${teamMap}" />
			</form:select>
		</c:if>
		<br>
		<br>
		<input type="submit" value="Back" name="_back" />
		<input type="submit" value="Register" name="_register" />
	</form:form>
</body>
</html>
