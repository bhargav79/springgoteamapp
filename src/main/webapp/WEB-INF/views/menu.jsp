<%@ taglib prefix="form" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="menu">
	<ul>
		<li><form:url value="/menu/homepage" var="url" htmlEscape="true" />
			<a href="${url}">Home</a></li>
		<br />
		<li><form:url value="/menu/match_nomination" var="url"
				htmlEscape="true" /> <a href="${url}">Match Nomination</a></li>
		<br />
		<li><form:url value="/menu/team_details" var="url"
				htmlEscape="true" /> <a href="${url}">Team Details</a></li>
		<br />
		
		<li><form:url value="/menu/add_message" var="url"
				htmlEscape="true" /> <a href="${url}">Send a Message</a></li>
			<br />
		<c:if test="${adminStatus}">
			<li><form:url value="/menu/add_match" var="url"
					htmlEscape="true" /> <a href="${url}">Add a Match</a></li>

			<br />
			<li><form:url value="/menu/team_selection" var="url"
					htmlEscape="true" /> <a href="${url}">Team Selection</a></li>
			<br />
			<li><form:url value="/menu/add_team" var="url" htmlEscape="true" />
				<a href="${url}">Add a Team</a></li>

			<br />

			<li><form:url value="/menu/make_admin" var="url"
					htmlEscape="true" /> <a href="${url}">Make Admin</a></li>
			<br />
		</c:if>
		<li><form:url value="/menu/logout" var="url" htmlEscape="true" />
			<a href="${url}">Log out</a></li>
	</ul>
</div>