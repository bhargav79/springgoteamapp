<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<
<script type="text/javascript">
	function disableLocation() {
		var venue = document.getElementById('venue');
		if (venue == 'neutral') {
			document.getElementById('location').style.visibility = 'visible';
			document.getElementById('locationLabel').style.visibility = 'visible';
		} else {
			document.getElementById('location').style.display = 'none';
			document.getElementById('locationLabel').style.display = 'none';
		}
	}
</script>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Add Match</h2>
		<br />
		<p>${matchStatus}</p>

		<form:form id="nominationForm" method="post" action="add_match_submit"
			commandName="matchBean">

			<form:errors path="*" />

			<c:if test="${not empty teamMap}">
				<form:label path="teamId">Please select the opponent*</form:label>
				<form:select path="teamId">
					<form:option value="" >....</form:option>
					<form:options items="${teamMap}" />
				</form:select>
				<form:errors path="teamId" />
			</c:if>

			<br>
			<br>
			<form:label path="venue">Please select venue*</form:label>
			<form:select path="venue">
				<form:option value="" >....</form:option>
				<form:options items="${venueMap}" />
			</form:select>
			<form:errors path="venue" />
			<br>
			<br>
			<form:label path="location">if, neutral</form:label>
			<form:input name="location" path="location" />
			<form:errors path="location" />
			<br>
			<br>
			<form:label path="schedule">Please select schedule*</form:label>
			<form:input name="schedule" path="schedule" id="date" />
			<form:errors path="schedule" />
			<br>
			<br>
			<br>
			<input type="submit" value="Submit" />
		</form:form>

	</tiles:putAttribute>
</tiles:insertDefinition>