<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Team Selection</h2>
		<br />
		<p>${selectionStatus}</p>

		<form:form id="GetDetailsForm" method="post"
			action="team_selection_submit" commandName="nominationBean">

			<form:select path="matchId">
				<form:option value="">....</form:option>
				<form:options items="${matchDetails}" />
			</form:select>
			<br />
			<br />
			<input type="submit" value="Get Details" name="_getDetails" />
			<br />
			<br />
			<c:if test="${not empty nominatedUsers or not empty selectedUsers}">
				<table>
					<tr>
						<c:if test="${not empty nominatedUsers}">
							<td><form:select path="userIds" multiple="true">
									<form:options items="${nominatedUsers}" />
								</form:select></td>
						</c:if>
						<td></td>
						<c:if test="${not empty selectedUsers}">
							<td><form:select path="userIds" multiple="true">
									<form:options items="${selectedUsers}" />
								</form:select></td>
						</c:if>
					</tr>
					<tr>
						<td><br /> <br /></td>
					</tr>
					<tr>
						<c:if test="${not empty nominatedUsers}">
							<td><input type="submit" value="Select" name="_selection" /></td>
						</c:if>
						<td></td>
						<c:if test="${not empty selectedUsers}">
							<td><input type="submit" value="De-Select"
								name="_deselection" /></td>
						</c:if>
					</tr>
				</table>
			</c:if>
		</form:form>
		<br />
	</tiles:putAttribute>
</tiles:insertDefinition>