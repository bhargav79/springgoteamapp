<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Add Message</h2>
		<br />
		<p>${matchStatus}</p>
		<p>${test}</p>

		<form:form id="nominationForm" method="post" action="add_message_submit"
			commandName="messageBean">

			<form:errors path="*" />

			<form:label path="message">Enter Message*</form:label>
			<form:input name="message" path="message" />
			<form:errors path="message" />
			<br>
			<br>
			<input type="submit" value="Submit"/>
		</form:form>

	</tiles:putAttribute>
</tiles:insertDefinition>