<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<title>Go Team</title>
</head>
<body>
	<h1 align="center">Go Team</h1>
	<br />
	<p>${loginStatus}</p>

	<form:form id="loginForm" method="post" action="login"
		commandName="userLogin">

		<form:label path="userName">Enter your user-name</form:label>
		<form:input name="userName" path="userName" />
		<br>
		<form:label path="password">Please enter your password</form:label>
		<form:password name="password" path="password" />
		<br><br>
		<input type="submit" value="Login" name="_login" />
		<input type="submit" value="Register" name="_register" />
	</form:form>
</body>
</html>
