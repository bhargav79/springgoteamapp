<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Add Team</h2>
		<br />
		<p>${teamStatus}</p>

		<form:form id="teamForm" method="post" action="add_team_submit"
			commandName="teamBean">

			<form:errors path="*" />

			<form:label path="teamName">Enter Team Name*</form:label>
			<form:input name="teamName" path="teamName" />
			<form:errors path="teamName" />
			<br>
			<br>
			<form:label path="teamCode">Enter Team Code*</form:label>
			<form:input name="teamCode" path="teamCode" />
			<form:errors path="teamCode" />
			<br>
			<br>
			<form:label path="location">Enter Location*</form:label>
			<form:input name="location" path="location" />
			<form:errors path="location" />
			<br>
			<br>
			<input type="submit" value="Submit" />
		</form:form>

	</tiles:putAttribute>
</tiles:insertDefinition>