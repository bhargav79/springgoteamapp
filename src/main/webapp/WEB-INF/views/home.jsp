<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Available Matches</h2>
        <h2 align=center> </h2>
		<form:form id="registrationForm" method="post" action="register"
			commandName="nominationBean">
			<c:if test="${not empty homeDTOs}">
				<table>
					<tr>
						<th></th>
						<th>Match Details</th>
						<th>Date</th>
					</tr>


					<c:forEach var="homeDTO" items="${homeDTOs}" varStatus="status">

						<tr>
							<td>${homeDTO.id}</td>
							<td>${homeDTO.awayTeam} @ ${homeDTO.homeTeam}</td>
							<td>${homeDTO.schedule}</td>
						</tr>
					</c:forEach>


				</table>
			</c:if>
			<br />
			<br />
			<c:if test="${not empty messageInfos}">
				<table>
					<tr>
						<th>User Name</th>
						<th>Message</th>
					</tr>


					<c:forEach var="messageInfo" items="${messageInfos}" varStatus="status">

						<tr>
							<td>${messageInfo.userInfo.userName}</td>
							<td>${messageInfo.message}</td>
						</tr>
					</c:forEach>


				</table>
			</c:if>
		</form:form>

	</tiles:putAttribute>
</tiles:insertDefinition>