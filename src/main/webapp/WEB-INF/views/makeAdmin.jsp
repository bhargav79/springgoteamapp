<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Admin Access</h2>
		<br />
		<p>${adminAccessStatus}</p>

		<form:form id="nominationForm" method="post"
			action="make_admin_submit" commandName="userBean">

			<form:label path="userId">Please select the user*</form:label>
			<form:select path="userId">
				<form:option value="">....</form:option>
				<form:options items="${userDetails}" />
			</form:select>
			<br />
			<br />
			<input type="submit" value="Submit" />
		</form:form>

	</tiles:putAttribute>
</tiles:insertDefinition>