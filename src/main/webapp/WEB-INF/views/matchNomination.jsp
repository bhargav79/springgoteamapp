<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="base-definition">
	<tiles:putAttribute name="body">

		<h2>Match Nomination</h2>
		<p>${nominationStatus}</p>

		<form:form id="nominationForm" method="post"
			action="match_nomination_submit" commandName="nominationBean">

			<form:label path="matchId">Please select the match*</form:label>
			<form:select path="matchId">
				<form:option value="">....</form:option>
				<form:options items="${matchDetails}" />
			</form:select>
			<br />
			<br />
			<form:radiobutton path="nomination" value="yes" /> Yes &nbsp;&nbsp;
			<form:radiobutton path="nomination" value="no" /> No
			<br />
			<input type="submit" value="Nominate" name="_nominate" />
			<input type="submit" value="Get Details" name="_getdetails" />
			<br />
			<br />
			<c:if test="${not empty selectedUsers}">
				<table>
					<tr>
						<td><form:select path="userIds" multiple="true"
								disabled="true">
								<form:options items="${selectedUsers}" />
							</form:select></td>
					</tr>
				</table>
			</c:if>
		</form:form>

	</tiles:putAttribute>
</tiles:insertDefinition>